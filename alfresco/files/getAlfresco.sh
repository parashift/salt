#!/bin/bash


if ( ! getopts ":u:p:v:" opt); then
	echo "Usage: `basename $0` -u <user> -p <pass> -v <version>";
	exit $E_OPTERROR;
fi


while getopts ":u:p:v:" opt; do
  case $opt in
    u)
      echo "Username is: $OPTARG"
      USER=$OPTARG
      ;;
    p)
      echo "Password is: $OPTARG"
      PASS=$OPTARG
      ;;
    v)
      echo "Version is: $OPTARG"
      VERSION=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit $E_OPTERROR;
      ;;
  esac
done

if [ ! -f alfresco.war ]; then
    echo "Downloading Alfresco"
    curl -u $USER:$PASS https://artifacts.alfresco.com/nexus/content/groups/private/org/alfresco/alfresco/$VERSION/alfresco-$VERSION.war > alfresco.war
  else
    echo "Alfresco Already Downloaded"
fi

if [ ! -f share.war ]; then
    echo "Downloading Share"
    curl -u $USER:$PASS https://artifacts.alfresco.com/nexus/content/groups/private/org/alfresco/share/$VERSION/share-$VERSION.war > share.war
  else
    echo "Share Already Downloaded"
fi

if [ ! -f solr.zip ]; then
    echo "Downloading Solr"
    curl -u $USER:$PASS https://artifacts.alfresco.com/nexus/content/groups/private/org/alfresco/alfresco-solr/$VERSION/alfresco-solr-$VERSION.zip > solr-alfresco.zip
  else
    echo "Solr Already Downloaded"
fi
echo "Finished!"
