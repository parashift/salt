{% from "alfresco/map.jinja" import imagemagick with context %}
{% set alfresco_version = salt['pillar.get']('alfresco_version','4.2.4') %}
{% set alfresco_dir = salt['pillar.get']('alfresco_dir','/srv/alfresco') %}


include:
  - tomcat
  - postgres

{% if salt['pillar.get']('modules_for_alfresco:solr', False) == True %}
  - alfresco.solr
{% endif %}

{% if pillar['kerberos'] is defined %}
  - alfresco.kerberos
{% endif %}

{% if pillar['ldap'] is defined and pillar['ldap'] is not mapping %}
  - alfresco.ldapchain
{% endif %}

{% if pillar['modules_for_alfresco']['license'] is defined %}
  - alfresco.license
{% endif %}

{% if salt['grains.get']('os_family') == "RedHat" %}

install_libreoffice:
    cmd.run:
        - cwd: /tmp
        - name: |
            curl http://mirror.aarnet.edu.au/pub/tdf/libreoffice/stable/4.4.4/rpm/x86_64/LibreOffice_4.4.4_Linux_x86-64_rpm.tar.gz -o LibreOffice.tgz
            tar xvzf LibreOffice.tgz
            cd LibreOffice_*_Linux_x86-64_rpm/RPMS
            ls | grep -vE 'gnome|kde' | xargs yum install -y
        - unless: rpm -qa |grep libreoffice4.4

/usr/lib/libreoffice:
    file.symlink:
        - target: /opt/libreoffice4.4
        - require:
            - cmd: install_libreoffice

swftools:
  pkg.installed:
    - pkgs:
      - libjpeg-turbo
      - zlib
      - giflib
      - freetype

{% else %}

libreoffice:
  pkgrepo.managed:
    - ppa: libreoffice/libreoffice-4-4
  pkg.installed:
    - require:
      - pkgrepo: libreoffice

swftools:
   pkg.installed


{% endif %}

/opt/swftools/pdf2swf:
   file.managed:
      - source: salt://alfresco/files/pdf2swf.{{grains['os_family']}}.{{grains['osrelease']}}
      - makedirs: True
      - mode: 755

{{imagemagick.pkg}}:
   pkg.installed

alfresco:
    postgres_user.present:
      - password: alfresco
      - require:
         - sls: postgres
    postgres_database.present:
      - owner: alfresco
      - require:
         - postgres_user: alfresco


/var/lib/tomcat7/webapps/ROOT/index.jsp:
   file.managed:
      - source: salt://alfresco/files/index.jsp
      - require:
         - service: tomcat7_enabled

/var/lib/tomcat7/webapps/ROOT/index.html:
   file.absent

/var/lib/tomcat7/shared/classes/alfresco-global.properties:
    file.managed:
      - user: tomcat7
      - group: tomcat7
      - template: jinja
      - source: 
        - salt://alfresco/files/alfresco-global.properties.{{ grains['id'] }}
        - salt://alfresco/files/alfresco-global.properties
      - makedirs: True

/var/lib/tomcat7/shared/classes/alfresco/web-extension/share-config-custom.xml:
  file.managed:
    - user: tomcat7
    - group: tomcat7
    - template: jinja
    - source:
      - salt://alfresco/files/share-config-custom.xml
    - makedirs: True

{% if pillar['ldap'] is defined and pillar['ldap'] is mapping %}
/var/lib/tomcat7/shared/classes/alfresco/extension/subsystems/Authentication/ldap-ad:
  file.absent
{% endif %}

{{alfresco_dir}}:
   file.directory:
      - user: tomcat7
      - group: tomcat7

{{alfresco_dir}}/alfresco-mmt.jar:
  file.managed:
    - source: salt://alfresco/files/alfresco-mmt.jar
    - mode: 755
    - require:
      - file : {{alfresco_dir}}


{% for webapp in [{'app': 'alfresco', 'dir': 'amps', 'type': 'repo'} , {'app':'share', 'dir': 'amps_share', 'type': 'share'}] %}

{{alfresco_dir}}/{{webapp.app}}.war:
   file.managed:
      - user: tomcat7
      - group: tomcat7
      - source: salt://alfresco/files/{{webapp.app}}.war.{{alfresco_version}}
      - require:
        - file : {{alfresco_dir}}

{% for module in salt['pillar.get']('modules_for_alfresco:' + webapp.type, []) %}

{{alfresco_dir}}/{{webapp.dir}}/{{module}}:
   file.managed:
   - user: tomcat7
   - source: salt://alfresco/files/{{webapp.dir}}/{{module}}
   - makedirs: True

# Install addtional Media Management Packages
{% if module.find('alfresco-mm-repo') >= 0 %}

{% if salt['grains.get']('os_family') == "RedHat" %}

/etc/yum.repos.d/nux-dextop.repo:
  file.managed:
    - source: salt://alfresco/files/nux-dextop.repo

ffmpeg:
  pkg.installed:
    - require: 
       - file: /etc/yum.repos.d/nux-dextop.repo

curl:
  pkg.installed

activemq-bin:
  cmd.run:
    - cwd: /opt
    - name: |
        curl http://apache.uberglobalmirror.com/activemq/5.11.1/apache-activemq-5.11.1-bin.tar.gz > apache-activemq-5.11.1-bin.tar.gz
        tar xvzf apache-activemq-5.11.1-bin.tar.gz
        ln -sf /opt/apache-activemq-5.11.1 /opt/apache-activemq
    - unless: test -e apache-activemq-5.11.1  && test -L apache-activemq

/etc/init.d/activemq:
  file.managed:
    - source: salt://alfresco/files/activemq.sh
    - mode: 766

activemq-service:
  cmd.run:
    - name: |
        chkconfig --add activemq
        chkconfig activemq on
    - unless: chkconfig | grep activemq
    - require:
        - file: /etc/init.d/activemq

activemq:
  service.running:
    - enable: True
    - reload: True
    - require:
        - cmd: activemq-service

perl-Image-ExifTool:
  pkg.installed

{% else %}
activemq:
  pkg:
    - installed
  service.running:
    - watch: 
        - file: /etc/activemq/instances-enabled/main
    - require:
        - file: /etc/activemq/instances-enabled/main

/etc/activemq/instances-enabled/main:
  file.symlink:
    - target: /etc/activemq/instances-available/main
    - require: 
        - pkg: activemq

ffmpeg:
   pkgrepo.managed:
     - ppa: mc3man/trusty-media
   pkg.installed:
     - require:
       - pkgrepo: ffmpeg

exiftool:
  pkg.installed
{% endif %}

{% endif %}
{% if module.find('alfresco-s3-connector') >= 0 %}
/mnt/cache:
  file.directory:
  - user: tomcat7
  - group: tomcat7
  - recurse:
    - user
    - group
  - makedirs: True
{% endif %}
{% endfor %}

{% endfor %}
