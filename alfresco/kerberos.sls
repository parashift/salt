{% if pillar['kerberos'] is defined %}
{% set jre_dir = salt['cmd.run']("readlink -f `which java` | sed -r 's/\/bin\/java//g'")%}

/etc/krb5.conf:
  file.managed:
    - source: salt://alfresco/files/krb5.conf
    - template: jinja

{{jre_dir}}/lib/security/java.login.config:
  file.managed:
    - source: salt://alfresco/files/java.login.config
    - template: jinja

{{jre_dir}}/lib/security/java.security:
  file.append:
    - text: login.config.url.1=file:{{jre_dir}}/lib/security/java.login.config

{% for svc in ['cifs','http'] %}

/etc/alfresco_{{svc}}.keytab.64:
  file.managed:
    - contents_pillar: kerberos:{{svc}}_keytab
    - mode: 600

/etc/alfresco_{{svc}}.keytab:
   cmd.wait:
     - name: base64 -d /etc/alfresco_{{svc}}.keytab.64 > /etc/alfresco_{{svc}}.keytab
     - watch:
       - file: /etc/alfresco_{{svc}}.keytab.64
   file.managed:
    - replace: False
    - mode: 600

{% endfor %}

{% endif %}


