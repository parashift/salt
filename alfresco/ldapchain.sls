{% for ldap_chain in pillar['ldap'] %}
create_auth_properties{{loop.index}}:
  file.managed:
    - name: /var/lib/tomcat7/shared/classes/alfresco/extension/subsystems/Authentication/ldap-ad/ldap{{loop.index}}/ldap-ad-authentication.properties
    - source: salt://alfresco/files/ldap-ad-authentication.properties
    - makedirs: true
    - user: tomcat7
    - group: tomcat7
    - template: jinja
    - context:
        loopIndex: {{loop.index0}}

{% endfor %}

