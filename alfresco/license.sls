{% if pillar['modules_for_alfresco']['license'] is defined %}
{% set lic_dir = "/var/lib/tomcat7/shared/classes/alfresco/extension/license" %}

{{lic_dir}}:
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - makedirs: True

{{lic_dir}}/alfresco.lic.64:
  file.managed:
    - contents_pillar: modules_for_alfresco:license
    - require:
       - file: {{lic_dir}}

change_license:
  cmd.wait:
    - name: base64 -d {{lic_dir}}/alfresco.lic.64 > {{lic_dir}}/alfresco.lic 
    - watch:
       - file: {{lic_dir}}/alfresco.lic.64

{% endif %}
