include:
  - tomcat
{% set alfresco_dir = salt['pillar.get']('alfresco_dir','/srv/alfresco') %}

unzip:
  pkg.installed

{% set alfresco_version = salt['pillar.get']('alfresco_version','4.2.4') %}

{% if alfresco_version == "4.2.4" %}

alfresco_download:
  file.managed:
    - name: /var/tmp/alfresco.zip
    - source: http://eu.dl.alfresco.com/release/enterprise/4.2/4.2.4-build-00082/alfresco-enterprise-4.2.4.zip
    - source_hash: md5=cb689101ce805d592ee16f33d52ba72e

alfresco_extract:
  cmd.run:
    - name: unzip /var/tmp/alfresco.zip web-server/webapps/alfresco.war web-server/webapps/share.war
    - cwd: /var/tmp/
    - unless: test -d /var/tmp/web-server/
    - require:
      - file: alfresco_download

{% for webapp in ['alfresco', 'share'] %}

{{alfresco_dir}}/{{webapp}}.war:
   file.copy:
      - user: tomcat7
      - group: tomcat7
      - force: True
      - Makedirs: True
      - source: /var/tmp/web-server/webapps/{{webapp}}.war

{% endfor %}

{% if salt['pillar.get']('modules_for_alfresco:solr', False) == True %}

/var/lib/tomcat7/solr-alfresco/:
  archive.extracted:
    - source: http://eu.dl.alfresco.com/release/enterprise/4.2/4.2.4-build-00082/alfresco-enterprise-solr-4.2.4.zip
    - source_hash: md5=15e12f4bcdedbe1b7c07491edb8aac38
    - archive_format: zip
    - require:
      - pkg: unzip
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - require:
      - archive: /var/lib/tomcat7/solr-alfresco/
    - recurse:
      - user
      - group

{% endif %}

{% endif %}

{% if alfresco_version == "5.0.1" %}

alfresco_download:
  file.managed:
    - name: /var/tmp/alfresco.zip
    - source: http://eu.dl.alfresco.com/release/enterprise/5.0/5.0.1-build-00068/alfresco-enterprise-5.0.1.zip
    - source_hash: md5=034b75063739139ff7660c514f7f0f7c

alfresco_extract:
  cmd.run:
    - name: unzip /var/tmp/alfresco.zip alfresco-enterprise-5.0.1/web-server/webapps/alfresco.war alfresco-enterprise-5.0.1/web-server/webapps/share.war
    - cwd: /var/tmp/
    - unless: test -d /var/tmp/alfresco-enterprise-5.0.1/
    - require:
      - file: alfresco_download

{% for webapp in ['alfresco', 'share'] %}

{{alfresco_dir}}/{{webapp}}.war:
   file.copy:
      - user: tomcat7
      - group: tomcat7
      - force: True
      - makedirs: True
      - source: /var/tmp/alfresco-enterprise-5.0.1/web-server/webapps/{{webapp}}.war

{% endfor %}


{% if salt['pillar.get']('modules_for_alfresco:solr', False) == True %}

solr_extract:
  cmd.run:
    - name: unzip /var/tmp/alfresco.zip alfresco-enterprise-5.0.1/web-server/webapps/solr4.war
    - cwd: /var/tmp/
    - unless: test -f /var/tmp/alfresco-enterprise-5.0.1/web-server/webapps/solr4.war
    - require:
      - file: alfresco_download

/var/lib/tomcat7/webapps/solr4.war:
   file.copy:
      - user: tomcat7
      - group: tomcat7
      - force: True
      - Makedirs: True
      - source: /var/tmp/alfresco-enterprise-5.0.1/web-server/webapps/solr4.war

{% endif %}

{% endif %}

{% if alfresco_version == "5.0.1.5" %}
alfresco_download:
  file.managed:
    - name: /var/tmp/alfresco.zip
    - source: http://eu.dl.alfresco.com/release/enterprise/5.0/5.0.1.5-build-00087/alfresco-enterprise-5.0.1.5.zip
    - source_hash: md5=c9052a2f1b868db08dcfd35ccc081ed1

{% endif %}
