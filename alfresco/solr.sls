{% if salt['pillar.get']('modules_for_alfresco:solr', False) == True %}
{% set alfresco_version = salt['pillar.get']('alfresco_version', '4.2.4') %}
{% set alfresco_dir = salt['pillar.get']('alfresco_dir','/srv/alfresco') %}

unzip:
  pkg.installed

{% if alfresco_version.startswith('5') %}
{% set servlet = 'solr4' %}

/var/lib/tomcat7/solr-alfresco/:
  file.recurse:
    - source: salt://alfresco/files/solr4

/var/lib/tomcat7/webapps/solr4.war:
  file.managed:
    - source: salt://alfresco/files/solr4.war
    - require:
      - file: /var/lib/tomcat7/conf/Catalina/localhost/{{servlet}}.xml
      - file: /var/lib/tomcat7/solr-alfresco/

{% else %}
{% set servlet = 'solr' %}

/var/lib/tomcat7/solr-alfresco/:
  archive.extracted:
    - source: salt://alfresco/files/alfresco-solr.zip.{{alfresco_version}}
    - archive_format: zip
    - require:
      - pkg: unzip
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - require:
      - archive: /var/lib/tomcat7/solr-alfresco/
    - recurse:
      - user
      - group

{% endif %}

/var/lib/tomcat7/conf/Catalina/localhost/{{servlet}}.xml:
  file.managed:
    - makedirs: true
    - source: salt://alfresco/files/solr-context.xml.{{alfresco_version}}
    - template: jinja


{% for location in ['workspace','archive'] %}

/var/lib/tomcat7/solr-alfresco/{{location}}-SpacesStore/conf/solrcore.properties:
  file.managed:
    - source: salt://alfresco/files/{{location}}-solrcore.properties.{{alfresco_version}}
    - template: jinja
    - require:
      - file: /var/lib/tomcat7/solr-alfresco/
    - require_in:
      - file: /var/lib/tomcat7/conf/Catalina/localhost/{{servlet}}.xml

{% endfor %}


/var/lib/tomcat7/conf/server.xml:
  file.managed:
    - require_in:
       - service: tomcat7_service
    - source: salt://alfresco/files/solr-server.xml
    - template: jinja

{{alfresco_dir}}/alf_data/keystore:
  file.recurse:
    - source: salt://alfresco/files/keystore
    - user: tomcat7
    - group: tomcat7
    - require_in:
       - service: tomcat7_service
{% endif %}
