{% set alfresco_dir = salt['pillar.get']('alfresco_dir','/srv/alfresco') %}
{% set solr_enabled = salt['pillar.get']('modules_for_alfresco:solr', False) %}


include:
 - alfresco

tomcat7-stopped:
  service.dead:
    - name: tomcat7
  require:
    - sls: alfresco
    - service: tomcat7_enabled


kill_soffice:
  cmd.run:
    - name: pkill soffice.bin
    - onlyif: ps ax | grep -v grep | grep -q soffice.bin

{% for webapp in [{'app': 'alfresco', 'dir': 'amps', 'type': 'repo'} , {'app':'share', 'dir': 'amps_share', 'type': 'share'}] %}

/var/lib/tomcat7/webapps/{{webapp.app}}/:
  file.absent:
    - require:
      - file: /var/lib/tomcat7/webapps/{{webapp.app}}.war
      - service: tomcat7-stopped
    - require_in:
      - service: tomcat7_service

/var/lib/tomcat7/webapps/{{webapp.app}}.war:
  file.copy:
    - source: {{alfresco_dir}}/{{webapp.app}}.war
    - force: True
    - require:
       - file: {{alfresco_dir}}/{{webapp.app}}.war
       - service: tomcat7-stopped
    - require_in:
       - file: /var/lib/tomcat7/webapps
       - service: tomcat7_service

{% if pillar['modules_for_alfresco']is defined %}

{% if pillar['modules_for_alfresco'][webapp.type] is defined %}

remove_symlink_dir_{{webapp.type}}:
   file.absent:
     - name: /var/tmp/mmt/{{webapp.dir}}

install_amps_{{webapp.type}}:
   cmd.run:
    - user: tomcat7
    - name: java -jar {{alfresco_dir}}/alfresco-mmt.jar install /var/tmp/mmt/{{webapp.dir}} /var/lib/tomcat7/webapps/{{webapp.app}}.war -directory -nobackup -force
    - require:
      - sls: alfresco
      - file: /var/lib/tomcat7/webapps
      - file: /var/tmp/mmt/{{webapp.dir}}
    - require_in:
      - service: tomcat7_service

/var/tmp/mmt/{{webapp.dir}}:
   file.directory:
     - makedirs: true
     - require:
       - file : remove_symlink_dir_{{webapp.type}}

{% for module in salt['pillar.get']('modules_for_alfresco:' + webapp.type, []) %}

/var/tmp/mmt/{{webapp.dir}}/{{module}}:
  file.symlink:
    - target: {{alfresco_dir}}/{{webapp.dir}}/{{module}}
    - require_in: 
      - cmd: install_amps_{{webapp.type}}

{% for prereq in ['rm-server','dynamicextensionsalfresco','alfresco-rm-share'] %}

{% if module.find(prereq) >= 0 %}
install_{{prereq}}:
   cmd.run:
    - user: tomcat7
    - name: java -jar {{alfresco_dir}}/alfresco-mmt.jar install /var/tmp/mmt/{{webapp.dir}}/{{module}} /var/lib/tomcat7/webapps/{{webapp.app}}.war -nobackup -force
    - require:
      - file: /var/lib/tomcat7/webapps
      - file: {{alfresco_dir}}/{{webapp.dir}}/{{module}}
      - file: /var/tmp/mmt/{{webapp.dir}}/{{module}}
    - require_in:
      - cmd: install_amps_{{webapp.type}}

{% endif %}

{% endfor %}

{% endfor %}

{% endif %}

{% endif %}

{% endfor %}

/var/lib/tomcat7/webapps:
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - recurse:
        - user
        - group
