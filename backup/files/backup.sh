#!/bin/bash

set -e
set -o pipefail

#Alfresco Hot backup script for eBoard

#Copies the database, the lucene backup directory, the configuration to S3 for greater redundancy, every hour

#Checks to see if /mnt/backup/ exists, because if it doesn't then the backup is broken.

if [ -d /mnt/backup/ ]
then
    sudo -i -u postgres pg_dumpall | gzip -9 > /mnt/backup/db_dump.`date +%u`.gz
    /usr/local/bin/aws s3 sync /mnt/backup/ s3://{{aws.default_bucket}}/backup/
else
    echo "Directory not found!"
fi
