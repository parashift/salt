awscli:
  pkg.installed

/mnt/backup:
  file.directory

/root/.aws/config:
  file.managed:
    - source: salt://backup/files/awsconfig
    - template: jinja
    - makedirs: True
    - context:
        aws: {{ pillar['aws'] }}

/opt/backup/backup.sh:
  file.managed:
    - source: salt://backup/files/backup.sh
    - template: jinja
    - makedirs: True
    - mode: 755
    - context:
        aws: {{ pillar['aws'] }}

/etc/cron.d/backup:
  file.managed:
    - source: salt://backup/files/cron_backup
    - require:
      - file: /opt/backup/backup.sh
      - file: /root/.aws/config
