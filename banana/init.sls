include:
  - tomcat

/var/lib/tomcat7/webapps/banana.war:
  file.managed:
    - source: salt://banana/files/banana.war
    - user: tomcat7
    - group: tomcat7