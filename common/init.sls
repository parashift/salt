{% from "common/map.jinja" import snmp with context %}

git:
  pkg.installed

ntp:
  pkg.installed

{% if salt['grains.get']('os_family') == "Debian" %}

python-software-properties:
  pkg.installed

/etc/default/snmpd:
  file.managed:
    - source: salt://common/files/snmp-defaults
    - require:
      - pkg: snmpd
    - watch_in:
      - service : snmp_install

{% endif %}

snmp_install:
  pkg:
    - name: {{snmp.pkg}}
    - installed
  service:
    - name: {{snmp.srv}}
    - running
    - watch:
      - file: /etc/snmp/snmpd.conf

snmp_service_enable:
  service:
     - name: {{snmp.srv}}
     - enabled
     - require:
       - pkg: snmp_install

/etc/snmp/snmpd.conf:
  file.managed:
    - source: salt://common/files/snmpd.conf
    - template: jinja
    - makedirs: True
    - require: 
      - pkg: snmp_install

/usr/bin/distro:
  file.managed:
    - source: salt://common/files/snmp-distro
    - mode: 755

/etc/ntp.conf:
  file.managed:
    - source: salt://common/files/ntp.conf


{% for sysctl in ['vm.swappiness', 'net.ipv6.conf.all.disable_ipv6', 'net.ipv6.conf.default.disable_ipv6', 'net.ipv6.conf.lo.disable_ipv6'] %}
{{sysctl}}:
  sysctl.present:
    - value: 1
{% endfor %}
