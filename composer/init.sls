include:
  - hhvm

get-composer:
  cmd.run:
    - name: 'CURL=`which curl`; $CURL -sS https://getcomposer.org/installer | hhvm --php -c /etc/hhvm/server.ini'
    - unless: test -f /usr/local/bin/composer
    - cwd: /root/

install-composer:
  cmd.wait:
    - name: mv /root/composer.phar /usr/local/bin/composer.phar
    - cwd: /root/
    - watch:
      - cmd: get-composer

/usr/local/bin/composer:
  file.managed:
    - source: salt://composer/files/composer
    - mode: 755