include:
 - composer

composer global require drush/drush:dev-master:
  cmd.run

composer_binaries:
  file.append:
    - name: /root/.profile
    - text: export PATH="$HOME/.composer/vendor/bin:$PATH"
