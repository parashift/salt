/etc/udev/rules.d/53-ec2-network-interfaces.rules:
  file.managed:
    - source: salt://ec2net/files/53-ec2-network-interfaces.rules
    - mode: 644

/etc/udev/rules.d/75-persistent-net-generator.rules:
  file.managed:
    - source: salt://ec2net/files/75-persistent-net-generator.rules
    - mode: 644

/etc/dhcp/dhclient-exit-hooks.d/ec2dhcp:
  file.managed:
    - source: salt://ec2net/files/ec2dhcp
    - mode: 644

/etc/network/ec2net-functions:
  file.managed:
    - source: salt://ec2net/files/ec2net-functions
    - mode: 644

/etc/network/ec2net.hotplug:
  file.managed:
    - source: salt://ec2net/files/ec2net.hotplug
    - mode: 644