include:
  - node
  - common
  - postgres

etherpad:
    postgres_user.present:
      - password: etherpad
      - require:
         - pkg: postgres
    postgres_database.present:
      - owner: etherpad
      - require:
         - postgres_user: etherpad

/usr/local/etherpad-lite:
   file.directory:
     - user: etherpad-lite
     - recurse:
       - user
     - require:
       - user: etherpad-lite
     - require_in: 
       - git: etherpad-git

etherpad-upstart:
   file.managed:
     - name: /etc/init/etherpad-lite.conf
     - source: salt://etherpad-lite/files/etherpad-lite.conf

etherpad-git:
  git.latest:
    - name: https://github.com/ether/etherpad-lite
    - target: /usr/local/etherpad-lite
    - user: etherpad-lite

etherpad-settings:
    file.managed:
     - name: /usr/local/etherpad-lite/settings.json
     - source: salt://etherpad-lite/files/settings.json
     - require:
        - git: etherpad-git

etherpad-apikey:
  file.managed:
    - name: /usr/local/etherpad-lite/APIKEY.txt
    - source: salt://etherpad-lite/files/APIKEY.txt
    - template: jinja
    - require:
        - git: etherpad-git
    - context:
        apikey : {{salt['pillar.get']('etherpad:api', "CHANGEME")}}

etherpad-lite:
     user.present:
      - shell: /bin/false
      - createhome: True
     service:
      - running
      - require:
        - git: etherpad-git
        - file: etherpad-upstart
        - file: etherpad-settings
        - user: etherpad-lite
        - postgres_database: etherpad
        - postgres_user: etherpad
      - watch:
        - git: etherpad-git
        - file: etherpad-settings
        - file: etherpad-apikey

