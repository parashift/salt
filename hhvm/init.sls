hhvm:
  pkgrepo.managed:
    - key_url: http://dl.hhvm.com/conf/hhvm.gpg.key
    - name: deb http://dl.hhvm.com/ubuntu trusty main
  pkg.installed:
    - require:
      - pkgrepo: hhvm
  service:
    - running
    - require:
      - pkg: hhvm
  alternatives.install:
    - name: php
    - path: /usr/bin/hhvm
    - priority: 10
    - link: /etc/alternatives/php

/etc/hhvm/server.ini:
  file.managed:
    - source: salt://hhvm/files/server.ini
    - require:
       - pkg: hhvm
    - watch_in:
       - service: hhvm

