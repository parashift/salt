include:
  - postgres
  - tomcat

jira:
  postgres_user.present:
    - password: jira
    - require:
      - pkg: postgres
  postgres_database.present:
    - owner: jira
    - require:
      - postgres_user: jira
  user.present:
    - home: /srv/atlassian/jira_home
    - createhome: True
    - require:
      - file: /srv/atlassian

/srv/atlassian:
  file.directory:
  - user: tomcat7
  - group: tomcat7
  - recurse:
    - user
    - group
  - makedirs: True

/srv/atlassian/jira_install:
  archive.extracted:
    - source: https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-6.4.9-war.tar.gz
    - source_hash: md5=7a38a9412eaac60ecd1e798d78325176
    - archive_format: tar
    - tar_options: v 
    - if_missing: /srv/atlassian/jira_install/atlassian-jira-6.4.9-war

set_jira_home:
  file.managed:
    - user: tomcat7
    - name: /srv/atlassian/jira_install/atlassian-jira-6.4.9-war/edit-webapp/WEB-INF/classes/jira-application.properties
    - source: salt://jira/files/jira-application.properties
    - require:
       - archive: /srv/atlassian/jira_install

build_jira:
  cmd.run:
    - cwd: /srv/atlassian/jira_install/atlassian-jira-6.4.9-war/
    - name: /srv/atlassian/jira_install/atlassian-jira-6.4.9-war/build.sh
    - unless: test -f /srv/atlassian/jira_install/atlassian-jira-6.4.9-war/dist-tomcat/atlassian-jira-6.4.9.war
    - require:
       - file: set_jira_home

/usr/share/tomcat7/lib:
  file.recurse:
    - source: salt://jira/files/lib

/srv/atlassian/jira_home/dbconfig.xml:
  file.managed:
    - user: tomcat7
    - source: salt://jira/files/dbconfig.xml
    - require:
      - file: /srv/atlassian
      - user: jira

/var/lib/tomcat7/conf/Catalina/localhost/jira.xml:
  file.managed:
    - user: tomcat7
    - source: salt://jira/files/jira.xml

/var/lib/tomcat7/webapps/jira.war:
  file.copy:
    - source: /srv/atlassian/jira_install/atlassian-jira-6.4.9-war/dist-tomcat/atlassian-jira-6.4.9.war
    - user: tomcat7
    - group: tomcat7
    - require: 
       - cmd: build_jira
       - file: /srv/atlassian/jira_home/dbconfig.xml
       - file: /var/lib/tomcat7/conf/Catalina/localhost/jira.xml

