mysql:
  pkg.installed:
    - pkgs:
      - mysql-server
      - mysql-client
      - python-mysqldb
