nodejs:
  pkgrepo.managed:
    - ppa: chris-lea/node.js
  pkg.installed:
    - require:
      - pkgrepo: nodejs
