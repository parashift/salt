<?php

$config->custom->appearance['friendly_attrs'] = array(
	'facsimileTelephoneNumber' => 'Fax',
	'gid'                      => 'Group',
	'mail'                     => 'Email',
	'telephoneNumber'          => 'Telephone',
	'uid'                      => 'User Name',
	'userPassword'             => 'Password'
);

$servers = new Datastore();

$servers->newServer('ldap_pla');

$servers->setValue('server','name','Test LDAP Server');

?>