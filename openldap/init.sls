slapd_setpassword:
  debconf:
    - set
    - data:
        'slapd/password1' : {'type': 'password', 'value': 'setup'}
        'slapd/password2' : {'type': 'password', 'value': 'setup'}

slapd:
  pkg:
    - installed
  debconf:
    - set
    - data:
        'slapd/backend' : {'type': 'select', 'value': 'HDB'}
        'slapd/no_configuration' : {'type' : 'boolean', 'value': False }
        'slapd/domain' : {'type' : 'string', 'value': 'PARASHIFT.local'}
        'shared/organization' : {'type' : 'string', 'value': 'PARASHIFT.local'}
        'slapd/move_old_database' : {'type' : 'boolean', 'value': True }
        'slapd/purge_database' : {'type' : 'boolean', 'value': False }
        'slapd/allow_ldap_v2' : {'type' : 'boolean', 'value': False }
    - require:
      - debconf: slapd_setpassword


ldap-utils:
  pkg.installed

reconfigure_slapd:
  cmd.wait:
    - name: dpkg-reconfigure -f noninteractive slapd
    - watch:
      - debconf: slapd

/opt/ldapadmin:
  file.directory:
    - user: www-data

phpldapadmin:
  git.latest:
    - name: https://github.com/leenooks/phpLDAPadmin
    - target: /opt/ldapadmin
    - user: www-data
    - require:
      - file: /opt/ldapadmin

/opt/ldapadmin/config/config.php:
  file.managed:
    - source: salt://openldap/files/config.php
    - user: www-data
    - require:
      - git: phpldapadmin

php5-fpm:
  pkg:
    - installed
  service:
    - running

php5-ldap:
  pkg.installed:
    - require:
      - pkg: php5-fpm

/etc/php5/fpm/pool.d/www.conf:
  file.managed:
    - source: salt://openldap/files/www.conf
    - watch_in:
      - service: php5-fpm


{% for mod in ['rewrite', 'proxy', 'proxy_http', 'proxy_fcgi'] %}

enable_mod_{{mod}}:
  cmd.run:
    - name: a2enmod {{mod}}
    - unless: a2query -m {{mod}}
    - watch_in:
      - service: apache2

{% endfor %}


apache2:
  pkg:
    - installed
  service:
    - running


/etc/apache2/sites-enabled/ldapadmin.conf:
  file.managed:
    - source: salt://openldap/files/ldapadmin.conf
    - watch_in:
      - service: apache2
