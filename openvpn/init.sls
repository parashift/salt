openvpn:
  pkg.installed:
    - pkgs:
      - openvpn
  file.recurse:
    - name: /etc/openvpn
    - source: salt://openvpn/files/openvpn
    - require:
      - pkg: openvpn
  service:
    - running
    - watch:
      - file: openvpn

openvpn_enabled:
  service:
     - name: openvpn
     - enabled
     - require:
        - pkg: openvpn