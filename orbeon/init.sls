{% set orbeon_version = salt['pillar.get']('orbeon:version','4.9.0.201505052303-PE') %}
{% set resources_dir = salt['pillar.get']('orbeon:resources_dir', '/opt/orbeon/resources') %}
{% set orb_comp = orbeon_version.split('.') %}
{% if orb_comp[2] != '0' %}
{% set orb_tag = orb_comp[0] + '.' + orb_comp[1] + '.' + orb_comp[2] + '-ce' %}
{% else %}
{% set orb_tag = orb_comp[0] + '.' + orb_comp[1] + '-ce' %}
{% endif %}

include:
  - tomcat
  - postgres

orbeon:
    postgres_user.present:
      - password: orbeon
      - require:
         - pkg: postgres
    postgres_database.present:
      - owner: orbeon
      - require:
         - postgres_user: orbeon

/opt/orbeon/orbeon.sql:
  file.managed:
    - makedirs: True
    - user: tomcat7
    - group: tomcat7
    - source: salt://orbeon/files/orbeon.sql

psql -f /opt/orbeon/orbeon.sql -d orbeon:
  cmd.run:
     - user: postgres
     - unless: psql -d orbeon -c "\d" | grep -q orbeon
     - require:
         - postgres_database: orbeon

orbeon-server:
  archive.extracted:
      - name: /opt/orbeon
      - archive_user: tomcat7
      - source: https://github.com/orbeon/orbeon-forms/releases/download/tag-release-{{orb_tag}}/orbeon-{{orbeon_version}}.zip
      - source_hash: https://github.com/orbeon/orbeon-forms/releases/download/tag-release-{{orb_tag}}/orbeon-{{orbeon_version}}.zip.MD5
      - archive_format: zip
      - if_missing: /opt/orbeon/orbeon-{{orbeon_version}}

{% if salt['pillar.get']('orbeon:license') is defined %}

{{resources_dir}}/config/license.xml:
  file.managed:
    - contents_pillar: orbeon:license
    - user: tomcat7
    - group: tomcat7
    - makedirs: True
    - require:
        - archive: orbeon-server

{% endif %}    


{% for form in salt['pillar.get']('orbeon:forms', []) %}

/opt/orbeon/forms/{{form.app}}_{{form.form}}.sql.gz:
  file.managed:
    - source: salt://orbeon/files/forms/{{form.app}}_{{form.form}}.sql.gz
    - makedirs: True

upload_{{form.app}}_{{form.form}}:
  cmd.wait:
    - user: postgres
    - name: zcat /opt/orbeon/forms/{{form.app}}_{{form.form}}.sql.gz | psql -d orbeon
    - require:
       - file: /opt/orbeon/forms/{{form.app}}_{{form.form}}.sql.gz
       - postgres_database: orbeon
    - watch:
       - file: /opt/orbeon/forms/{{form.app}}_{{form.form}}.sql.gz

{% endfor %}

{{resources_dir}}/config/properties-local.xml:
  file.managed:
    - template: jinja
    - makedirs: true
    - source: salt://orbeon/files/properties-local.xml
    - user: tomcat7
    - group: tomcat7
    - require:
        - archive: orbeon-server

{{resources_dir}}/config/log4j.xml:
  file.managed:
    - makedirs: True
    - source: salt://orbeon/files/log4j.xml
    - user: tomcat7
    - group: tomcat7
    - require:
        - archive: orbeon-server

/var/lib/tomcat7/conf/Catalina/localhost/orbeon.xml:
  file.managed:
    - source: salt://orbeon/files/orbeon.xml
    - user: tomcat7
    - group: tomcat7
    - template: jinja
    - context:
        orbeon_version: {{orbeon_version}}
        resources_dir: {{resources_dir}}

/var/lib/tomcat7/webapps/orbeon.war:
  file.copy:
    - user: tomcat7
    - group: tomcat7
    - force: True
    - require:
      - file: /var/lib/tomcat7/conf/Catalina/localhost/orbeon.xml
      - archive: orbeon-server
    - source: /opt/orbeon/orbeon-{{orbeon_version}}/orbeon.war
