include:
  - apache

/opt/phabricator:
  file.directory:
    - user: www-data
    - group: www-data

{% for mod in ['ssl', 'rewrite', 'proxy', 'proxy_http'] %}

enable_mod_{{mod}}:
  cmd.run:
    - name: a2enmod {{mod}}
    - unless: a2query -m {{mod}}
    - watch_in:
      - service: apache2

{% endfor %}

phabricator:
  pkg.installed:
    - pkgs:
        - git
        - python-mysqldb
        - mysql-server
        - apache2
        - dpkg-dev
        - mysql-server
        - php5
        - php5-mysql
        - php5-gd
        - php5-dev
        - php5-curl
        - php-apc
        - php5-cli
        - php5-json

/etc/apache2/sites-enabled/phab.conf:
  file.managed:
    - source: salt://phabricator/files/apache_phab.conf
    - require:
       - pkg: phabricator
    - watch_in:
       - service: apache2

{% for mod in ['libphutil', 'arcanist', 'phabricator'] %}

https://github.com/phacility/{{mod}}.git:
  git.latest:
    - target: /opt/phabricator/{{mod}}

{% endfor %}


install_db:
  cmd.run:
     - name: ./bin/storage upgrade --force
     - cwd: /opt/phabricator/phabricator
     - unless: mysqlshow | grep -q phabricator
     - require:
        - git: https://github.com/phacility/phabricator.git
        - pkg: phabricator
