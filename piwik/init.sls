include:
 - apache
 - mysql
 - hhvm
 - composer


piwik:
  mysql_user.present:
    - password: piwik
  mysql_database.present:
    - owner: piwik
    - require:
       - mysql_user: piwik
  mysql_grants.present:
    - grant: all privileges
    - database: piwik.*
    - user: piwik
    - require:
       - mysql_database: piwik

/opt/piwik:
  file.directory:
    - user: www-data


https://github.com/piwik/piwik.git:
  git.latest:
    - rev: 2.9.1
    - target: /opt/piwik
    - user: www-data
    - require:
      - file: /opt/piwik
