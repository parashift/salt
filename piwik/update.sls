include:
  - piwik

composer install:
  cmd.run:
    - cwd: /opt/piwik


a2enmod proxy_fcgi:
  cmd.run

a2enmod rewrite:
  cmd.run
