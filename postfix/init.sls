postfix:
  pkg:
    - installed
  service:
    - running
    - require:
       - pkg: postfix
  debconf.set:
    - data:
        'postfix/mailname': {'type': 'string', 'value': {{salt['pillar.get']('external_domain', "localhost")}}}
        'postfix/main_mailer_type' : { 'type' : 'select', 'value' : "Internet Site" }
    - require_in:
      - pkg: postfix
  cmd.wait:
    - name: dpkg-reconfigure -f noninteractive postfix
    - watch:
       - debconf : postfix
    - require:
      - pkg: postfix


{% if salt['pillar.get']('modules_for_alfresco:imap', False) == True %}
alfresco:
  alias.present:
    - target: /dev/null
{% endif %}