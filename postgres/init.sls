{% set postgres = salt['grains.filter_by']({
    'Debian': {'pkg': 'postgresql', 'srv': 'postgresql'},
    'RedHat': {'pkg': 'postgresql94-server', 'srv': 'postgresql-9.4' },
}, default='Debian') %}



postgres:
    pkg.installed:
        - name: {{ postgres.pkg }}
    service.running:
        - name: {{ postgres.srv }}


{% if salt['grains.get']('os_family') == "RedHat" %}

{% set pg_data = salt['pillar.get']('pg_data', '/var/lib/pgsql/9.4') %}
{{pg_data}}:
   file.directory:
     - user: postgres
     - group: postgres
     - recurse:
        - user
        - group
     - makedirs: True

{{pg_data}}/data/pg_hba.conf:
    file.managed:
        - source: salt://postgres/files/pg_hba.conf
        - user: postgres
        - group: postgres
        - watch_in:
          - service: postgres
        - mode: 600

pgdg94:
  pkgrepo.managed:
    - humanname: PostgreSQL 9.4 $releasever - $basearch
    - baseurl: http://yum.postgresql.org/9.4/redhat/rhel-$releasever-$basearch
    - gpgcheck: 1
    - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG-94
    - require:
      - file: pgdg94_gpg
    - require_in:
      - pkg: postgres

pgdg94_gpg:
  file.managed:
    - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG-94
    - source: salt://postgres/files/rpm_gpg

systemd_file:
  file.managed:
    - name: /etc/systemd/system/postgresql-9.4.service
    - source: salt://postgres/files/postgresql-9.4.service
    - template: jinja
    - context:
        pg_data: {{pg_data}} 

postgresql-initdb:
  cmd.run:
    - cwd: /
    - user: root
    - name: /usr/pgsql-9.4/bin/postgresql94-setup initdb
    - unless: test -f {{pg_data}}/data/postgresql.conf
    - require_in:
      - service: postgres
      - file: {{pg_data}}/data/pg_hba.conf
    - env:
      LC_ALL: C.UTF-8
    - require:
        - file: systemd_file
        - file: {{pg_data}}

{% endif %}
