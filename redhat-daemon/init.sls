#installs authbind and start-stop-daemon for redhat, so that tomcat actually works
# authbind can be found here: git://git.chiark.greenend.org.uk/~ian/authbind.git
# start-stop-daemon needs to be compiled from dpkg with configure && make: http://security.debian.org/debian-security/pool/updates/main/d/dpkg/dpkg_1.16.16.tar.xz
# This script will fail if there are no files for the particular os_family/release combination

{% for daemonFile in ['/usr/local/lib/authbind/libauthbind.so.1', '/usr/bin/authbind', '/usr/bin/start-stop-daemon'] %}

#The fancy function 'daemonFile.split('/')[-1]' there just gets the file from the redhad-daemon/files/ directory, as we don't include the full path
# i.e, /usr/local/lib/authbind/libauthbind.so.1 => libauthbind.so.1

{{daemonFile}}:
  file.managed:
    - makedirs: True
    - mode: 755
    - source: salt://redhat-daemon/files/{{daemonFile.split('/')[-1]}}.{{grains['os_family']}}.{{grains['osrelease']}}

{% endfor %}

{% if grains['osrelease'] > 7 %}

redhat-lsb-core:
  pkg.installed

{% endif %}