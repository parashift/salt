#!/bin/bash


if ( ! getopts ":v:" opt); then
	echo "Usage: `basename $0` -v <version>";
	exit $E_OPTERROR;
fi


while getopts ":v:" opt; do
  case $opt in
    v)
      echo "Version is: $OPTARG"
      VERSION=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit $E_OPTERROR;
      ;;
  esac
done

if [ ! -f /var/tmp/solr-$VERSION.zip ]; then
    echo "Downloading Solr"
    curl http://mirror.ventraip.net.au/apache/lucene/solr/$VERSION/solr-$VERSION.zip > /var/tmp/solr-$VERSION.zip
    unzip /var/tmp/solr-$VERSION.zip -d /var/tmp/
  else
    echo "Solr Already Downloaded"
fi

if [ ! -f solr.war ]; then
    echo "Getting solr war"
    cp /var/tmp/solr-$VERSION/dist/solr-$VERSION.war ./solr.war
fi

if [ ! -d solr ]; then
    echo "Creating Extracted Solr Home Directory"
    rsync -a --exclude='collection1' /var/tmp/solr-$VERSION/example/solr/ ./solr/
    rsync -a --exclude='*.war' /var/tmp/solr-$VERSION/dist ./solr/
    rsync -a /var/tmp/solr-$VERSION/contrib ./solr/
fi

if [ ! -d ext ]; then
   echo "Creating ext lib directory"
   rsync -a /var/tmp/solr-$VERSION/example/lib/ext ./
   rsync -a /var/tmp/solr-$VERSION/example/resources/ ./ext/
fi

if [ ! -d cores ]; then
   echo "Creating cores"
   rsync -a /var/tmp/solr-$VERSION/example/solr/collection1 ./cores/
   sed -i 's/<lib dir=\"\.\.\/\.\.\/\.\.\//<lib dir=\"\.\.\//g' cores/collection1/conf/solrconfig.xml
fi
echo "Finished!"
