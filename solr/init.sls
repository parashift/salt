include:
  - tomcat

/var/lib/tomcat7/webapps/solrext.war:
  file.managed:
    - source: salt://solr/files/solr.war
    - user: tomcat7
    - group: tomcat7


/var/lib/tomcat7/conf/Catalina/localhost/solrext.xml:
  file.managed:
    - source: salt://solr/files/solr-context.xml


{% for dir in ['solr','solr_extra'] %}

recurse_{{dir}}:
  file.recurse:
    - name: /var/lib/tomcat7/solr
    - source: salt://solr/files/{{dir}}
    - user: tomcat7
    - group: tomcat7

{% endfor %}

/var/lib/tomcat7/solr/ext:
  file.recurse:
    - source: salt://solr/files/ext
    - user: tomcat7
    - group: tomcat7
    - require:
      - file: /var/lib/tomcat7/solr

/srv/solr:
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - recurse:
      - user
      - group
    - makedirs: True
    - clean: False


# Includes any cores defined in the pillar.  The pillar should be defined as such:
#
# solr_cores:
#   - collection1

{% for core in salt['pillar.get']('solr_cores', ['collection1']) %}

/var/lib/tomcat7/solr/{{core}}:
  file.recurse:
    - source: salt://solr/files/cores/{{core}}
    - user: tomcat7
    - group: tomcat7
    - require:
      - file: /var/lib/tomcat7/solr

/etc/cron.d/solr-{{core}}:
  file.managed:
    - source: salt://solr/files/solr-cron
    - template: jinja
    - context:
        core: {{core}}
{% endfor %}
