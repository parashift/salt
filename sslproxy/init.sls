{% from "sslproxy/map.jinja" import apache with context %}

{{apache.pkg}}:
  pkg:
    - installed
  service:
    - running

{{apache.etc.dir}}/server.cert:
  file.managed:
    - source: salt://sslproxy/files/server.cert
    - template: jinja
    - context:
        cert: {{ pillar['sslproxy'] }}
    - require:
        - pkg: {{apache.pkg}}
    - watch_in:
      - service: {{apache.pkg}}

{{apache.etc.dir}}/intermediate.cert:
  file.managed:
    - source: salt://sslproxy/files/intermediate.cert
    - template: jinja
    - context:
        cert: {{ pillar['sslproxy'] }}
    - require:
        - pkg: {{apache.pkg}}
    - watch_in:
      - service: {{apache.pkg}}

{% for site in ['ssl-redirect','ssl-alfresco'] %}

{{apache.etc.site}}/{{site}}.conf:
  file.managed:
    - source: salt://sslproxy/files/{{site}}.conf
    - template: jinja
    - context:
        domain: {{salt['pillar.get']('external_domain', "localhost")}}
        path: {{apache.etc.dir}}
    - require:
      - pkg: {{apache.pkg}}
    - watch_in:
      - service: {{apache.pkg}}

{% if salt['grains.get']('os_family') == "Debian" %}
enable_site_{{site}}:
  cmd.run:
    - name: a2ensite {{site}}
    - unless: a2query -s {{site}}
    - watch_in:
      - service: apache2
    - require:
      - file: /etc/apache2/server.cert
      - file: /etc/apache2/intermediate.cert
      - file: /etc/apache2/sites-available/{{site}}.conf
{% endif %}
{% endfor %}

{% if salt['grains.get']('os_family') == "RedHat" %}

mod_ssl:
  pkg:
    - installed
    - watch_in:
      - service: {{apache.pkg}}

{{apache.etc.site}}/welcome.conf:
  file.absent:
  - watch_in:
    - service: {{apache.pkg}}

{% else %}

disable_default_site:
  cmd.run:
    - name: a2dissite 000-default
    - onlyif: a2query -s 000-default
    - watch_in:
      - service: apache2

{% for mod in ['ssl', 'rewrite', 'proxy', 'proxy_http'] %}

enable_mod_{{mod}}:
  cmd.run:
    - name: a2enmod {{mod}}
    - unless: a2query -m {{mod}}
    - watch_in:
      - service: apache2

{% endfor %}

{% if salt['pillar.get']('sslproxy:jira', Undefined) is defined %}
/etc/apache2/sites-enabled/ssl-jira.conf:
  file.managed:
    - source: salt://sslproxy/files/ssl-jira.conf
    - template: jinja
    - context:
        domain: {{salt['pillar.get']('sslproxy:jira')}}
    - require:
      - pkg: apache2
    - watch_in:
      - service: apache2
{% endif %}
{% endif %}
