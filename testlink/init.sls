include:
  - apache

testlink:
  pkg.installed:
    - pkgs:
        - git
        - python-mysqldb
        - mysql-server
        - apache2
        - dpkg-dev
        - php5
        - php5-mysql
        - php5-gd
        - php5-dev
        - php5-curl
        - php-apc
        - php5-cli
        - php5-json
  service.running:
    - name: mysql
  mysql_user.present:
    - password: testlink
    - require:
      - service: testlink
      - pkg: testlink
  mysql_database.present:
    - owner: testlink
    - require:
       - mysql_user: testlink
  mysql_grants.present:
    - grant: all privileges
    - database: testlink.*
    - user: testlink
    - require:
       - mysql_database: testlink

/opt/testlink:
  file.directory:
    - user: www-data
    - makedirs: True

https://github.com/TestLinkOpenSourceTRMS/testlink-code:
  git.latest:
    - target: /opt/testlink
    - user: www-data
    - require:
      - file: /opt/testlink

/var/testlink/logs:
  file.directory:
    - user: www-data
    - makedirs: True

/var/testlink/upload_area/:
  file.directory:
    - user: www-data
    - makedirs: True

/etc/apache2/sites-enabled/testlink.conf:
  file.managed:
    - source: salt://testlink/files/apache_testlink.conf
    - require:
       - pkg: testlink
    - watch_in:
       - service: apache2
