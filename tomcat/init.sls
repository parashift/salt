{% from "tomcat/map.jinja" import tomcat with context %}
{% set alfresco_version = salt['pillar.get']('alfresco_version', '4.2.4') %}

{% if salt['grains.get']('os_family') == "RedHat" %}

include:
  - redhat-daemon

/opt/java:
  file.directory

download_rpm:
  cmd.run:
    - name: curl -b oraclelicense=accept-securebackup-cookie -L http://download.oracle.com/otn-pub/java/jdk/8u45-b14/jdk-8u45-linux-x64.rpm > /opt/java/jdk-8u45-linux-x64.rpm
    - unless: test -f /opt/java/jdk-8u45-linux-x64.rpm
    - require:
      - file: /opt/java

java:
  pkg.installed:
    - sources:
      - jdk1.8.0_45: /opt/java/jdk-8u45-linux-x64.rpm
    - require:
      - cmd: download_rpm
  alternatives.set:
    - path: /usr/java/jdk1.8.0_45/jre/bin/java

create_tomcat7_dir:
  file.directory:
    - name: /var/lib/tomcat7
    - user: tomcat7
    - group: tomcat7
    - require:
      - user: tomcat7_user

download_tomcat7:
  cmd.run:
    - user: tomcat7
    - group: tomcat7
    - name: curl http://apache.mirror.digitalpacific.com.au/tomcat/tomcat-7/v7.0.63/bin/apache-tomcat-7.0.63.tar.gz | tar xvzf - -C /var/lib/tomcat7 --strip-components=1 --exclude='*/webapps/examples'
    - unless: test -f /var/lib/tomcat7/bin/catalina.sh
    - require:
      - file: create_tomcat7_dir

create_tomcat7_service:
    file.managed:
        - name: /etc/init.d/tomcat7
        - source: salt://tomcat/files/tomcat7.service
        - user: root
        - group: root
        - mode: 755

tomcat7_user:
    user.present:
        - name: tomcat7
        - fullname: Apache Tomcat 7
        - shell: /sbin/nologin
        - home: /usr/share/tomcat7


{% else %}

{% if alfresco_version.startswith('5') %}

{% set oraclepkg = 'oracle-java8-installer' %}
{% set javadir = 'java-8-oracle' %}

{% else %}

{% set oraclepkg = 'oracle-java7-installer' %}
{% set javadir = 'java-7-oracle' %}

{% endif %}

java:
  pkgrepo.managed:
    - ppa: webupd8team/java
  pkg.installed:
    - name: {{oraclepkg}}
    - watch_in:
      - service: tomcat7_enabled
    - require:
      - pkgrepo: java
  debconf.set:
    - name: {{oraclepkg}}
    - data:
        'shared/accepted-oracle-license-v1-1': {'type': 'boolean', 'value': True}
    - require_in:
      - pkg: java

/usr/lib/jvm/default-java:
   file.symlink:
      - target: /usr/lib/jvm/{{javadir}}
      - require_in:
         - service: tomcat7

tomcat7:
  pkg.installed:
     - require:
        - pkg: java

tomcat7-admin:
  pkg.installed:
     - require:
        - pkg: tomcat7

{% endif %}

tomcat7_enabled:
  service:
    - name: tomcat7
    - enabled
    - require:
{% if salt['grains.get']('os_family') == "RedHat" %}
        - file: create_tomcat7_service
{% else %}
        - pkg: tomcat7
{% endif %}

{{tomcat.lib}}/postgresql-9.3-1102-jdbc41.jar:
    file.managed:
      - source: salt://tomcat/files/postgresql-9.3-1102-jdbc41.jar
      - require_in:
         - service: tomcat7_service

tomcat7_service:
  service:
     - name: tomcat7
     - running
     - require:
       - service: tomcat7_enabled
     - watch:
        - file: /var/lib/tomcat7/conf/tomcat-users.xml
        - file: /etc/default/tomcat7
        - file: /var/lib/tomcat7/conf/catalina.properties

/var/lib/tomcat7/conf/catalina.properties:
   file.managed:
      - source: salt://tomcat/files/catalina.properties
      - require:
         - service: tomcat7_enabled

/var/lib/tomcat7/conf/tomcat-users.xml:
   file.managed:
      - source: salt://tomcat/files/tomcat-users.xml
      - require:
         - service: tomcat7_enabled
      - template: jinja

/etc/default/tomcat7:
   file.managed:
      - source: salt://tomcat/files/default-tomcat7
      - template: jinja

/var/lib/tomcat7:
  file.directory:
    - require:
        - service: tomcat7_enabled
    - user: tomcat7
    - group: tomcat7
    - recurse:
      - user
      - group
    - makedirs: False
    - clean: False
